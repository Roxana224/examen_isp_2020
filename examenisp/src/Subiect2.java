import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Subiect2 extends JFrame implements ActionListener {
    private JButton b1;
    private JTextField t1;
    private JTextArea t2;
    private JLabel l1,l2;
    Subiect2()
    { this.setSize(300,300);
        this.setLayout(null);
        b1=new JButton("Click!");
        b1.setBounds(100,200,100,40);
        b1.addActionListener(this);
        this.add(b1);
        t1=new JTextField();
        t1.setBounds(110,10,100,30);
        this.add(t1);
        t2=new JTextArea();
        t2.setBounds(110,50,100,30);
        this.add(t2);
        l1=new JLabel("Text field: ");
        l1.setBounds(10,10,100,30);
        this.add(l1);
        l2=new JLabel("Text area:");
        l2.setBounds(10,50,100,30);
        this.add(l2);
        this.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==b1)
        {
            if(t1.getText()!="") {
                File f = new File("C:\\Users\\Roxana\\Desktop\\" + t1.getText() + ".txt");
                FileWriter fr = null;
                try {
                    fr = new FileWriter(t1.getText() + ".txt");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                try {
                    fr.write(t2.getText());
                    fr.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            t1.setText("");
            t2.setText("");
        }
    }
    public static void main(String[] args)
    {
        new Subiect2();
    }


}

